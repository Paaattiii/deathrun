/*
* Deathrun is a plugin based off of the popular minigame TNTRUN.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.deathrun;

import java.util.ArrayList;
import java.util.Collection;

import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.objects.arenas.Arena;
import mc.alk.arena.objects.events.ArenaEventHandler;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

public class DeathrunArena extends Arena {
	ArrayList<BlockState> locations = new ArrayList<BlockState>();
	int task1, task2, campTimer;
	boolean timer;
  
	public void onOpen() {
		timer = false;
	}
  
	public void onStart() {
		timer = true;
		antiCamp();
	}
  
	public void onFinish() {
		Bukkit.getScheduler().cancelTask(task1);
		Bukkit.getScheduler().cancelTask(campTimer);
		task2 = Bukkit.getScheduler().scheduleSyncDelayedTask(Deathrun.getSelf(), new Runnable() {
			public void run() {
				regenLayers();
				locations.clear();
				Bukkit.getScheduler().cancelTask(task2);
			}
		}, 40L);
	}
  
  
	private void antiCamp() {
		campTimer = Bukkit.getScheduler().scheduleSyncRepeatingTask(Deathrun.getSelf(), new Runnable(){
			public void run() {
				Collection<ArenaPlayer> players = getMatch().getAlivePlayers();
				for (ArenaPlayer ap: players){
					Location ploc = ap.getPlayer().getLocation();
					ploc.setY(ploc.getY() - 1);
					Location l = getPlayerStandOnBlockLocation(ploc);
					blockDisappear(l);
				}
			}
		}, 2, 2);
	}

  
	private Location getPlayerStandOnBlockLocation(Location locationUnderPlayer) {
		Location b11 = locationUnderPlayer.clone().add(0.3,0,-0.3);
		if (b11.getBlock().getType() != Material.AIR) {
			return b11;
		}
		Location b12 = locationUnderPlayer.clone().add(-0.3,0,-0.3);
		if (b12.getBlock().getType() != Material.AIR) {
			return b12;
		}
		Location b21 = locationUnderPlayer.clone().add(0.3,0,0.3);
		if (b21.getBlock().getType() != Material.AIR) {
			return b21;
		}
		Location b22 = locationUnderPlayer.clone().add(-0.3,0,+0.3);
		if (b22.getBlock().getType() != Material.AIR) {
			return b22;
		}
		return locationUnderPlayer;
	}
  

	@ArenaEventHandler(suppressCastWarnings=true)
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Location loc = event.getPlayer().getLocation();
		Location newloc = new Location(loc.getWorld(), loc.getX(), loc.getY() - 1.0D, loc.getZ());

		if (newloc.getBlock().getType() == Material.STATIONARY_WATER && ! player.isDead()) {
			PlayerDeathEvent ede = new PlayerDeathEvent(player, new ArrayList<ItemStack>(), 0, "");
			Bukkit.getPluginManager().callEvent(ede);
		}
	} 
  
	public void blockDisappear(final Location loc) {
		if (timer) {
			task1 = Bukkit.getScheduler().scheduleSyncDelayedTask(Deathrun.getSelf(), new Runnable() {
				public void run() {
					BlockState state = loc.getBlock().getState();
					if (loc.getBlock().getType() != Material.AIR && !locations.contains(state)) {
						loc.getBlock().setType(Material.AIR);
						World world = loc.getWorld();
						world.playEffect(loc, Effect.MOBSPAWNER_FLAMES, 5);
						locations.add(state);
					}
				}
	        }, Defaults.delay);
		}
	}
  
	@ArenaEventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if ((event.getEntity() instanceof Player)) {
			Player player = (Player)event.getEntity();
			if (event.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
				event.setCancelled(true);
			}
			if ((event.getCause().equals(EntityDamageEvent.DamageCause.VOID)) || (event.getCause().equals(EntityDamageEvent.DamageCause.LAVA))) {
				PlayerDeathEvent ede = new PlayerDeathEvent(player, new ArrayList<ItemStack>(), 0, "");
				Bukkit.getPluginManager().callEvent(ede);
			}
		}
	}
	
	@ArenaEventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}
  
	@SuppressWarnings("deprecation")
	public void regenLayers() {
		for(BlockState state : locations) {
			state.getBlock().setType(state.getType());
			state.getBlock().setData(state.getBlock().getData());
			state.update(true);
		}
	}
  
	public Collection<ArenaPlayer> getArenaPlayer() {
		return getMatch().getAlivePlayers();
	}
}
