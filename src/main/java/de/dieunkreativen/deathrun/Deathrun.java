/*
* Deathrun is a plugin based off of the popular minigame TNTRUN.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.deathrun;

import java.util.logging.Logger;

import mc.alk.arena.BattleArena;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.configuration.file.FileConfiguration;

public class Deathrun extends JavaPlugin {
	static Deathrun plugin;
	Logger log;
	
	
    @Override
    public void onLoad() {
    	log = getLogger();
    }
    
	@Override
	public void onEnable() {
		plugin = this;
		PluginDescriptionFile pdfFile = this.getDescription();
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
		BattleArena.registerCompetition(this, "Deathrun", "dr", DeathrunArena.class);
		
        saveDefaultConfig();
        loadConfig();
	}
	@Override
	public void onDisable() {
		log.info("Deathrun is disabled!");
	
	}
	
	public static Deathrun getSelf() {
		return plugin;
	}
	
    @Override
    public void reloadConfig(){
    	super.reloadConfig();
    	if (loadConfig()) {
    		log.info("Your config.yml file is up to date");
    	}
    	else {
    		log.warning("Your config.yml file is not up to date. Please delete your current file and restart the server");
    	}
    }
	
    public boolean loadConfig(){
    	if (this.getConfig().getDouble("configVersion") == 1.2){
    		FileConfiguration config = getConfig();
            Defaults.delay = config.getInt("DelayInTicks");
            return true;
    	}
    	else {
    		return false;   
       }
        
    }
	
	
	
}
	

	

	



